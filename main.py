from tinyman.v1.client import TinymanMainnetClient
import configparser
import json
import requests
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import datetime


config = configparser.ConfigParser()

config.read("config.ini")

client = TinymanMainnetClient()

sender_email = config["EMAIL"]['sender_email']
receiver_emails = config["EMAIL"]['receiver_emails']
password = config["EMAIL"]['sender_password']

subject = "ASAWatcher"

message = MIMEMultipart("alternative")
message["From"] = sender_email
message["To"] = receiver_emails

api_url = config["API"]['api_url']

currency = config["LOCALIZATION"]["currency"].lower()

headers = {'content-type': 'application/json'}

html = """\
<html>
  <body>
"""

cryptoAlerts = {}
sep = ','

dateTimeFormat = "%m/%d/%Y %H:%M:%S"

error = False

def alert_triggered(symbol, alert):
    if symbol in cryptoAlerts:
        cryptoAlerts[symbol].append(alert)
    else:
        cryptoAlerts[symbol] = [alert]


try:
    response = requests.get(f"{api_url}/coins/algorand?localization=false&community_data=false&developer_data=false&tickers=false", headers=headers)
    json_data = json.loads(response.text)
    market_data = json_data["market_data"]
    algo_price = market_data["current_price"][currency]
    print(f'ALGO PRICE: {algo_price}')

    with open("alerts.json", "r+") as file:
        data = json.load(file)
        asaToWatch = data["asas"]

        for asa in asaToWatch:
            symbol = asa['symbol']
            #Fetch our two assets of interest
            ASA = client.fetch_asset(asa['assetId'])
            ALGO = client.fetch_asset(0)

            # Fetch the pool we will work with
            pool = client.fetch_pool(ASA, ALGO)

            # Get a quote for a swap of ALGO to 1 ASA with 1% slippage tolerance
            quote = pool.fetch_fixed_input_swap_quote(ASA(1 * 10 ** ASA.decimals), slippage=0.01)
            price = 0.0
            if 'divide' in asa:
                divide = asa['divide']
                price = (quote.price / divide) * algo_price
                print(f'{symbol} per ALGO: {price}')
                print(f'{symbol} per ALGO (worst case): {quote.price_with_slippage / divide}')
            else:
                price = quote.price * algo_price
                print(f'{symbol} per ALGO: {price}')
                print(f'{symbol} per ALGO (worst case): {quote.price_with_slippage}')
            for alert in asa["priceAlerts"]:
                if alert["active"] == True and alert["triggerEvery"] != "" and (alert["lastTriggered"] == "" or (alert["lastTriggered"] != ""
                 and (datetime.now() - datetime.strptime(alert["lastTriggered"], dateTimeFormat)).total_seconds() > alert["triggerEvery"])):
                    if alert["direction"] == 0:
                        if "price" in alert and price > alert["price"]:
                            alert_triggered(symbol, alert)
                            alert["lastTriggered"] = datetime.now().strftime(dateTimeFormat)
                            html += f"{symbol} price is above {alert['price']} at {price}"
                            html += "<br>"
                    else:
                        if "price" in alert and price < alert["price"]:
                            alert_triggered(symbol, alert)
                            alert["lastTriggered"] = datetime.now().strftime(dateTimeFormat)
                            html += f"{symbol} price is below {alert['price']} at {price}"
                            html += "<br>"
        file.seek(0)  # rewind
        json.dump(data, file, indent=4)
        file.truncate()


except Exception as e:
    print(e)
    error = True
    html += "<p>" + str(e) + "</p>"

html += """\
      </body>
</html>
"""

print(html)

if len(cryptoAlerts) > 0 or error == True:
    if error == True:
        subject += ": Error Occured!"
    else:
        if len(cryptoAlerts) > 0:
            subject += f": {sep.join(cryptoAlerts.keys())} triggered price threshold"

    message["Subject"] = subject

    part = MIMEText(html, "html")

    message.attach(part)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(config["EMAIL"]["smtp_host"], config["EMAIL"]["smtp_port"], context=context) as server:
        server.ehlo()
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_emails, message.as_string()
        )
else:
    print("No alerts triggered")





